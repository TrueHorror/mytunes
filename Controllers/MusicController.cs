﻿using Microsoft.AspNetCore.Mvc;
using MyTunes.Models.DataTransferObjects.MusicObjects;
using MyTunes.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Controllers
{
    ///<summary>
    /// Controller for /music API endpoint
    ///</summary>
    [ApiController]
    [Route("api/v1/music")]
    public class MusicController : ControllerBase
    {
        private readonly MusicRepository _musicRepository;

        public MusicController(MusicRepository musicRepository)
        {
            _musicRepository = musicRepository;
        }

        // GET: api/v1/music/random
        [HttpGet("random")]
        public ActionResult<RandomMusicDTO> GetRandomMusic()
        {
            // Fetch 5 random artists, genres and tracks from MusicRepository
            return Ok(_musicRepository.GetRandomMusic());
        }

        // GET: api/v1/music/search?track={query}
        [HttpGet("search")]
        public ActionResult<IEnumerable<TrackDTO>> GetSearchedTracks([FromQuery]string track)
        {
            if (track == "")
            {
                return BadRequest();
            }
            // Request tracks with track.Name matching string track from MusicRepository
            return Ok(_musicRepository.GetSearchedTracks(track));
        }
    }
}
