﻿using Microsoft.AspNetCore.Mvc;
using MyTunes.Models.DataTransferObjects;
using MyTunes.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Controllers
{
    ///<summary>
    /// Controller for /customer API endpoint
    ///</summary>
    [ApiController]
    [Route("api/v1/customers")]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerRepository _customerRepository;

        public CustomerController(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        // GET: api/v1/customers
        [HttpGet]
        public ActionResult<IEnumerable<CustomerDTO>> Get()
        {
            // Fetch all customers from CustomerRepository
            return Ok(_customerRepository.GetCustomers());
        }

        // GET: api/v1/customers/{id}
        [HttpGet("{id}")]
        public ActionResult<CustomerDTO> Get(int id)
        {
            var model = _customerRepository.GetCustomerById(id);

            if (model == null)
            {
                return NotFound("Customer not found");
            }

            // Fetch customer by id from CustomerRepository
            return Ok(model);
        }

        // POST: api/v1/customers
        [HttpPost]
        public ActionResult Post(CustomerDTO customer)
        {
            // Add new customer with CustomerRepository
            bool success = _customerRepository.AddNewCustomer(customer);
            return CreatedAtAction(nameof(Get), new { id = customer.CustomerId }, success);
        }

        // PUT: api/v1/customers/{id}
        [HttpPut("{id}")]
        public ActionResult Put(int id, CustomerDTO customer)
        {
            //Check for bad requesrt
            if (id != customer.CustomerId)
            {
                return BadRequest();
            }

            // Request update customer by id from CustomerRepository
            bool success = _customerRepository.UpdateCustomer(customer);
            return NoContent();
        }

        // GET: api/v1/customers/country
        [HttpGet("country")]
        public ActionResult<IEnumerable<CountryCountCustomersDTO>> GetCustomersInCountries()
        {
            var model = _customerRepository.GetCustomersInCountries();

            if (model == null)
            {
                return NotFound("Countries not found");
            }

            // Fetch list of countries with count of registered customers from CustomerRepository
            return Ok(model);
        }

        // GET: api/v1/customers/highest-spenders
        [HttpGet("highest-spenders")]
        public ActionResult<IEnumerable<HighestSpendersDTO>> GetHighestSpenders()
        {
            // Fetch customers with total money spent, ordered descending
            return Ok(_customerRepository.GetHighestSpenders());
        }


        // GET: api/v1/customers/{id}/popular-genre
        [HttpGet("{id}/popular/genre")]
        public ActionResult<IEnumerable<TopGenreDTO>> GetCustomerTopGenre(int id)
        {
            // Fetch customers top genre by customer id from CustomerRepository
            return Ok(_customerRepository.GetCustomerTopGenre(id));
        }
    }
}
