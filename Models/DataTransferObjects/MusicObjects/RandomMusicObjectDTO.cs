﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models.DataTransferObjects.MusicObjects
{
    ///<summary>
    /// Model for returned musicobjects(Artist, Genre, Track)
    /// containing the fields: Id, Name
    ///</summary>
    public class RandomMusicObjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
