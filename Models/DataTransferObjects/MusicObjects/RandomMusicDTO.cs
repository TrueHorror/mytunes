﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models.DataTransferObjects.MusicObjects
{
    ///<summary>
    /// Model for returning lists with 5 random artists, tracks and genres
    ///</summary>
    public class RandomMusicDTO
    {
        public List<RandomMusicObjectDTO> Artists { get; set;}
        public List<RandomMusicObjectDTO> Tracks { get; set; }
        public List<RandomMusicObjectDTO> Genres { get; set; }
    }
}
