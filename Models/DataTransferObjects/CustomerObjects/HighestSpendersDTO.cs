﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models.DataTransferObjects
{
    public class HighestSpendersDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double TotalSpent { get; set; }
    }
}
