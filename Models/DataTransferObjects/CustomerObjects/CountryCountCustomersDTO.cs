﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models.DataTransferObjects
{
    public class CountryCountCustomersDTO
    {
        public string Country { get; set; }
        public int CustomersCount { get; set; }
    }
}
