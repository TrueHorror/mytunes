﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Models.DataTransferObjects
{
    public class TopGenreDTO
    {
        public string Name { get; set; }
        public int GenreCount { get; set; }
    }
}
