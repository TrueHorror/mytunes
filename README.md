# MyTunes dotNet Assignment 3
## By Lars Lien and Fredrik Olsvold

## API Endpoints:
### Customers
* GET: api/v1/customers
	* Returns a list of customer data transfer objects `DTO's` in JSON format
* GET: api/v1/customers/{id}
	* Returns one `DTO` of � customer
* POST: api/v1/customers (Body: CustomerDTO)
	* Adds a customer to the database. Body has same properties as a `customerDTO`
* PUT: api/v1/customers/{id}
	* Updates a customer with a specified `id`
* GET: api/v1/customers/country
	* Returns a list of Countries with count of customers in that country
* GET: api/v1/customers/highest-spenders
	* Returns list of customers who has spent the most money, in decending order 
* GET: api/v1/customers/{id}/popular-genre
	* Returns most popular genre for a specified customer by `id`

### Music
* GET: api/v1/music/random
	* Returns 5 random artists, genres and tracks
* GET: api/v1/music/search?track={query}
	* Requests tracks with track.Name LIKE query from MusicRepository
	* Returns a list with TrackDTOs (results)

## Repositories
* CustomerRepository
	* Contains all SQL queries and operations called by the CustomerController
* MusicRepository
	* Contains all SQL queries and operations called by the MusicController
* DBConnection
	* Creates connection to local DB
	* Change variables dataSource and initialCatalogue to match your local database

## Utils folder.
Was originally an attempt to declare all repositories in `Startup.ConfigureServices()` instead of having to add every repository manually.

## Models
* Each model is denoted with DTO to clarify that the model does not represent the whole DB object.