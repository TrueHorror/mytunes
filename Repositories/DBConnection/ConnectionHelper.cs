﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Repositories
{
    ///<summary>
    /// Sets up connection to database
    ///</summary>
    public class ConnectionHelper
    {
        // Local database constants
        static string dataSource = "DESKTOP-8467RUV\\SQLEXPRESS";
        static string initialCatalogue = "Chinook";

        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = dataSource;
            connectionStringBuilder.InitialCatalog = initialCatalogue;
            connectionStringBuilder.IntegratedSecurity = true;

            return connectionStringBuilder.ConnectionString;
        }
    }
}
