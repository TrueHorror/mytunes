﻿using MyTunes.Models.DataTransferObjects.MusicObjects;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Repositories
{
    ///<summary>
    /// Handling all database logic and operations for MusicController
    ///</summary>
    public class MusicRepository
    {
        ///<summary>
        /// Fetch 5 random genres, artists and tracks from database
        ///</summary>
        public RandomMusicDTO GetRandomMusic()
        {
            RandomMusicDTO randomMusic = new RandomMusicDTO();
            randomMusic.Genres = GetRandomGenres();
            randomMusic.Artists = GetRandomArtist();
            randomMusic.Tracks = GetRandomTracks();
            return randomMusic;
        }

        ///<summary>
        /// Tries to fetch 5 random genres from the database
        ///</summary>
        private List<RandomMusicObjectDTO> GetRandomGenres()
        {
            string sql = "SELECT TOP 5 Genre.GenreId, Genre.Name " +
                "FROM Genre " +
                "ORDER BY NEWID();"; // NEWID() sorts the list randomly
            List<RandomMusicObjectDTO> genres = DBQueryRandomMusic(sql);
            return genres;
        }

        ///<summary>
        /// Tries to fetch 5 random artists from the database
        ///</summary>
        private List<RandomMusicObjectDTO> GetRandomArtist()
        {
            string sql = "SELECT TOP 5 Artist.ArtistId, Artist.Name " +
                "FROM Artist " +
                "ORDER BY NEWID();";
            List<RandomMusicObjectDTO> artists = DBQueryRandomMusic(sql);
            return artists;
        }

        ///<summary>
        /// Tries to fetch 5 random tracks from the database
        ///</summary>
        private List<RandomMusicObjectDTO> GetRandomTracks()
        {
            string sql = "SELECT TOP 5 Track.TrackId, Track.Name " +
                "FROM Track " +
                "ORDER BY NEWID();";
            List<RandomMusicObjectDTO> tracks = DBQueryRandomMusic(sql);
            return tracks;
        }

        ///<summary>
        /// Tries to fetch 5 random RandomMusicDTO from the database
        ///</summary>
        private List<RandomMusicObjectDTO> DBQueryRandomMusic(string sql)
        {
            List<RandomMusicObjectDTO> musicList = new List<RandomMusicObjectDTO>();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                RandomMusicObjectDTO music = new RandomMusicObjectDTO();
                                music.Id = reader.GetInt32(0);
                                music.Name = reader.GetString(1);
                                musicList.Add(music);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return musicList;
        }
        public List<TrackDTO> GetSearchedTracks(string query)
        {
            List<TrackDTO> tracks = new List<TrackDTO>();
            string sql = "SELECT Track.Name AS TrackName, Genre.Name AS GenreName, Artist.Name AS ArtistName, Album.Title AS AlbumName " +
                            "FROM Track " +
                            "INNER JOIN Album ON Album.AlbumId = Track.AlbumId " +
                            "INNER JOIN Artist ON Album.ArtistId = Artist.ArtistId " +
                            "INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                            "INNER JOIN MediaType ON MediaType.MediaTypeId=Track.MediaTypeId " +
                            "WHERE MediaType.MediaTypeId != 3 " + // No movies, only music tracks
                            "AND Track.Name LIKE @Query " +
                            "ORDER BY Track.Name; ";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Query", "%" + query + "%");
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TrackDTO track = new TrackDTO();
                                track.Name = reader.GetString(0);
                                track.Genre = reader.GetString(1);
                                track.Artist = reader.GetString(2);
                                track.Album = reader.GetString(3);
                                tracks.Add(track);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex.Message);
                throw ex;

            }

            return tracks;
        }
    }
}
