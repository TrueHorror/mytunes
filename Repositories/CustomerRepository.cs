﻿using MyTunes.Models;
using MyTunes.Models.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MyTunes.Repositories
{
    ///<summary>
    /// Handling all database logic and operations for CustomerController
    ///</summary>
    public class CustomerRepository
    {
        ///<summary>
        /// Request List of all customers from local database
        ///</summary>
        ///<returns> List of Customer objects</returns>
        public List<CustomerDTO> GetCustomers()
        {
            List<CustomerDTO> customerList = new List<CustomerDTO>();

            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerDTO customer = new CustomerDTO();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

            return customerList;

        }

        ///<summary>
        /// Request customer by id from local database
        ///</summary>
        ///<returns> One single Customer object</returns>
        public CustomerDTO GetCustomerById(int id)
        {
            CustomerDTO customer = new CustomerDTO();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.IsDBNull(3) ? null : reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

            return customer;
        }

        ///<summary>
        /// Sends request to add new customer to local database
        ///</summary>
        ///<returns> Success bool </returns>
        public bool AddNewCustomer(CustomerDTO customer)
        {

            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
                " VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

            return success;

        }

        ///<summary>
        /// Sends request to update existing customer in local database
        ///</summary>
        ///<returns> Success bool </returns>
        public bool UpdateCustomer(CustomerDTO customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName," +
                " Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email" +
                " WHERE CustomerId = @CustomerId";


            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;

            }

            return success;
        }

        ///<summary>
        /// Request number of customers in each country, ordered descending
        ///</summary>
        ///<returns> List of countries with count of customers </returns>

        public List<CountryCountCustomersDTO> GetCustomersInCountries()
        {
            List<CountryCountCustomersDTO> countList = new List<CountryCountCustomersDTO>();
            string sql = "SELECT Country, COUNT(*) AS CountCustomers " +
                "FROM Customer GROUP BY Country " +
                "ORDER BY CountCustomers DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CountryCountCustomersDTO countryCount = new CountryCountCustomersDTO();
                                countryCount.Country = reader.GetString(0);
                                countryCount.CustomersCount = reader.GetInt32(1);
                                countList.Add(countryCount);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;

            }

            return countList;
        }

        ///<summary>
        /// Sends request to get customers total spendings in decending order from local database.
        ///</summary>
        ///<returns> List of HighestSpendersDTO, customers with total spendings. </returns>
        public List<HighestSpendersDTO> GetHighestSpenders()
        {
            List<HighestSpendersDTO> highestSpenders = new List<HighestSpendersDTO>();
            string sql = "SELECT Customer.FirstName, Customer.LastName, Invoice.Total " +
                "FROM Invoice " +
                "INNER JOIN Customer ON Invoice.CustomerId=Customer.CustomerId " +
                "ORDER BY Invoice.Total DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                HighestSpendersDTO highSpender = new HighestSpendersDTO();
                                highSpender.FirstName = reader.GetString(0);
                                highSpender.LastName = reader.GetString(1);
                                highSpender.TotalSpent = (double)reader.GetDecimal(2);
                                highestSpenders.Add(highSpender);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;

            }

            return highestSpenders;
        }

        ///<summary>
        /// Request top genre of customer by customerId.
        ///</summary>
        ///<returns> List of TopGenreDTO objects with the highest count </returns>
        public List<TopGenreDTO> GetCustomerTopGenre(int id)
        {
            List<TopGenreDTO> topGenres = new List<TopGenreDTO>();
            // Selects top 1 results with ties( If any)
            string sql = "SELECT TOP 1 WITH TIES Genre.Name, COUNT(*) AS GenreCount " +
                "FROM Invoice " +
                "INNER JOIN InvoiceLine ON Invoice.InvoiceId=InvoiceLine.InvoiceId " +
                "INNER JOIN Track ON InvoiceLine.TrackId=Track.TrackId " +
                "INNER JOIN Genre ON Track.GenreId=Genre.GenreId " +
                "WHERE Invoice.CustomerId=@CustomerId " +
                "GROUP BY Genre.Name " +
                "ORDER BY GenreCount DESC;";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                TopGenreDTO topGenre = new TopGenreDTO();
                                topGenre.Name = reader.GetString(0);
                                topGenre.GenreCount = reader.GetInt32(1);
                                topGenres.Add(topGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;

            }

            return topGenres;
        }
    }
}
